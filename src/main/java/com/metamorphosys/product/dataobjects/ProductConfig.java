package com.metamorphosys.product.dataobjects;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "productEntityManagerFactory",
		transactionManagerRef = "productTransactionManager", basePackages={"com.metamorphosys.product.repository"})
public class ProductConfig {

	@Bean
	PlatformTransactionManager productTransactionManager() {
		return new JpaTransactionManager(productEntityManagerFactory().getObject());
	}

	@Bean
	LocalContainerEntityManagerFactoryBean productEntityManagerFactory() {

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setDatabase(Database.ORACLE);
		//vendorAdapter.setShowSql(true);
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setDataSource(productDataSource());
		factoryBean.setJpaVendorAdapter(vendorAdapter);
		factoryBean.setPackagesToScan(this.getClass().getPackage().getName());

		return factoryBean;
	}

	
	@Bean
	@ConfigurationProperties(prefix="master.spring.datasource")
	public DataSource productDataSource() {
	    return DataSourceBuilder.create().build();
	}

}
