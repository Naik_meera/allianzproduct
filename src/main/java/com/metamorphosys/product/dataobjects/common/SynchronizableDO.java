package com.metamorphosys.product.dataobjects.common;

import java.sql.Timestamp;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class SynchronizableDO extends BaseDO {

	public Timestamp systemCreatedDt;
	public Timestamp systemUpdatedDt;
	
	public Timestamp getSystemCreatedDt() {
		return systemCreatedDt;
	}
	public void setSystemCreatedDt(Timestamp systemCreatedDt) {
		this.systemCreatedDt = systemCreatedDt;
	}
	public Timestamp getSystemUpdatedDt() {
		return systemUpdatedDt;
	}
	public void setSystemUpdatedDt(Timestamp systemUpdatedDt) {
		this.systemUpdatedDt = systemUpdatedDt;
	}
	
}
