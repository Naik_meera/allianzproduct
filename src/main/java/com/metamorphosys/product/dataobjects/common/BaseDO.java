package com.metamorphosys.product.dataobjects.common;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public abstract class BaseDO {

	@Transient
	private BaseDO parentObject;
	
	public Integer versionId;
	public String guid;
	
	public void setParentObject(BaseDO parentObject)
	{
		this.parentObject = parentObject;
	}
	
	public BaseDO getParentObject()
	{
		return this.parentObject;
	}
	
	public Integer getVersionId() {
		return versionId;
	}

	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public abstract Long getId();
	
	public abstract String getObjectName();
	
}
