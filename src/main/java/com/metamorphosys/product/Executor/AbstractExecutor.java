package com.metamorphosys.product.Executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.metamorphosys.insureconnect.controllers.WebExecuteController;
import com.metamorphosys.insureconnect.controllers.WebGetController;
import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.jpa.master.ProductRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.product.dataobjects.common.ResultDO;
import com.metamorphosys.product.interfaceobjects.BaseIO;
import com.metamorphosys.product.interfaceobjects.CreateProductIO;
import com.metamorphosys.product.utility.SerializationUtility;


public abstract class AbstractExecutor {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractExecutor.class);
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	WebExecuteController webExecuteController;
	
	@Autowired
	WebGetController webGetController;
	
	public CreateProductIO init(CreateProductIO io) {
		io = callRuleEngine(io,"EXECUTE");
		LinkedTreeMap map= (LinkedTreeMap) io.getResultDO().getResultMap().get(io.getServiceDO().getService());
		io.getProductDO().setGuid((String) map.get("guid"));
		return io;
		
	}

	public CreateProductIO execute(CreateProductIO io) {
		io = callRuleEngine(io,"EXECUTE");
		productRepository.save(io.getProductDO());
		postExecute(io);
		return io;
	}
	
	public void postExecute(CreateProductIO io) {}

	public CreateProductIO load(CreateProductIO io) {
		
		io = callRuleEngine(io,"GET");
		return io;
	}
	
	private CreateProductIO callRuleEngine(CreateProductIO io,String method) {

		String tojson = covertTojson(io);
		LOGGER.info(tojson);
		ResponseEntity<String> jsonObject=null;
		if(InsureConnectConstants.Method.EXECUTE.equals(method)){
			jsonObject = webExecuteController.executeJson(tojson);
			
		}else if(InsureConnectConstants.Method.GET.equals(method)){
			jsonObject = webGetController.execute(tojson);
			//io.setProductDO(productRepository.findById(io.getProductDO().getId()));
		}
		/*RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(tojson.toString(), httpHeaders);
		ResponseEntity<String> jsonObject = restTemplate.exchange(url,
				HttpMethod.POST, entity, String.class);
*/		
		io = convertjsonToObject(jsonObject.getBody(), io,method);

		return io;
	}

	private CreateProductIO convertjsonToObject(String json, CreateProductIO io,String method) {

		String data = null;
		LinkedTreeMap map = SerializationUtility.getInstance().fromJson(json);

		if (null != map && map.containsKey("data")) {
			data = map.get("data").toString();
		}
		
		ArrayList<LinkedTreeMap> responseDataList = (ArrayList<LinkedTreeMap>) SerializationUtility.getInstance().fromJson(data, ArrayList.class);
		if (null != responseDataList) {
			for (LinkedTreeMap responseDataIO : responseDataList) {
				if (InsureConnectConstants.DataObjects.PRODUCTDO.equals(responseDataIO.get("objectName").toString())) {
					if(!InsureConnectConstants.Method.GET.equals(method)){
					ProductDO baseDO = (ProductDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ProductDO.class);
					io.setProductDO(baseDO);
					}else{
						io.setProductDO(productRepository.findById(io.getProductDO().getId()));
					}
					
				}
				if (InsureConnectConstants.DataObjects.SERVICEDO.equals(responseDataIO.get("objectName").toString())) {
					ServiceDO baseDO = (ServiceDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ServiceDO.class);

					io.setServiceDO(baseDO);
				}
				if (InsureConnectConstants.DataObjects.RESULTDO.equals(responseDataIO.get("objectName").toString())) {
					ResultDO baseDO = (ResultDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ResultDO.class);

					io.setResultDO(baseDO);
					setOvverideServiceData(io);
				}
				if (InsureConnectConstants.DataObjects.WORKFLOWSERVICEDO.equals(responseDataIO.get("objectName").toString())) {
					setOvverideServiceData(responseDataIO.get("object"),io);
				}

			}
		}
		return io;
	}
	
	private void setOvverideServiceData(CreateProductIO io) {
		if(io.getServiceDO().getServiceTaskList().size()>0 && io.getResultDO()!=null){
			LinkedTreeMap map = (LinkedTreeMap) io.getResultDO().getResultMap().get("WorkflowServiceDO");
			if(map!=null && map.size()>0){
				ArrayList<LinkedTreeMap> responseDataList = (ArrayList<LinkedTreeMap>) map.get("workflowServiceTaskList");
				for (LinkedTreeMap responseDataIO : responseDataList) {
					if(InsureConnectConstants.Status.PENDING.equals(responseDataIO.get("status"))){
						io.getServiceDO().getServiceTaskList().get(0).setServiceTask((String) responseDataIO.get("serviceTask"));
					}
				}
			}
			//
		}
		
	}

	private void setOvverideServiceData(Object object, CreateProductIO io) {
		JsonElement element = (JsonElement) SerializationUtility.getInstance().fromJson(object.toString(),JsonElement.class );
		JsonObject jsonResult = element.getAsJsonObject();
		JsonArray data = jsonResult.getAsJsonArray("workflowServiceTaskList");
		 for(int i = 0 ; i < data.size() ; i++) {
			 String status=data.get(i).getAsJsonObject().get("status").toString().replace("\"", "");
			 if(status.equals(InsureConnectConstants.Status.PENDING)){
				 if(io.getServiceDO()!=null){
					 io.getServiceDO().getServiceTaskList().get(0).setServiceTask(data.get(i).getAsJsonObject().get("serviceTask").toString().replace("\"", ""));
				 }else{
					 ServiceDO serviceDO = new ServiceDO();
					 serviceDO.setService(jsonResult.get("service").toString().replace("\"", ""));
					 serviceDO.setService(jsonResult.get("serviceType").toString().replace("\"", ""));
					 serviceDO.setService(data.get(i).getAsJsonObject().get("serviceTask").toString().replace("\"", ""));
					 io.setServiceDO(serviceDO);
				 }
			 }
			 //io.getServiceDO().getServiceTaskList().get(0).setServiceTask(serviceTask);
		 }
		/*if(object.getServiceDO().getServiceTaskList().size()>0 && object.getResultDO()!=null){
			LinkedTreeMap map = (LinkedTreeMap) object.getResultDO().getResultMap().get("WorkflowServiceDO");
			if(map!=null && map.size()>0){
				ArrayList<LinkedTreeMap> responseDataList = (ArrayList<LinkedTreeMap>) map.get("workflowServiceTaskList");
				for (LinkedTreeMap responseDataIO : responseDataList) {
					if("PENDING".equals(responseDataIO.get("status"))){
						object.getServiceDO().getServiceTaskList().get(0).setServiceTask((String) responseDataIO.get("serviceTask"));
					}
				}
			}
			//
		}*/
		
	}

	public String covertTojson(CreateProductIO io) {
		List<BaseIO> baseIOs = new ArrayList<BaseIO>();

		BaseIO baseIO = new BaseIO();
		baseIO.setObjectName(io.getProductDO().getObjectName());
		baseIO.setObject(SerializationUtility.getInstance().toJson(io.getProductDO()));
		baseIOs.add(baseIO);

		if(io.getServiceDO()!=null){
			BaseIO baseIO1 = new BaseIO();
			baseIO1.setObjectName(io.getServiceDO().getObjectName());
			baseIO1.setObject(SerializationUtility.getInstance().toJson(io.getServiceDO()));
			baseIOs.add(baseIO1);
		}

		String serialize = SerializationUtility.getInstance().toJson(baseIOs);
		HashMap<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("data", serialize);
		String finalString = SerializationUtility.getInstance().toJson(dataMap);
		

		return finalString;

	}

	public abstract ServiceDO getService();

}
