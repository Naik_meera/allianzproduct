package com.metamorphosys.product.Executor;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceTaskDO;
import com.metamorphosys.product.interfaceobjects.CreateProductIO;

@Component
public class ProductExecutor extends AbstractExecutor{

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductExecutor.class);
	
//	@Autowired
//	ProductConersionUtil productconversionUtil;
	
	@Override
	public CreateProductIO init(CreateProductIO io) {
		if(io.getServiceDO()==null){
			io.setServiceDO(getService());
		}
		super.init(io);
		return io;
	}
	
	@Override
	public CreateProductIO execute(CreateProductIO io) {
		ProductDO productDO = (ProductDO) io.getProductDO();
//		getClientID(productDO.getProductClientList());
//		getProductNum(productDO);
		
		super.execute(io);
		return io;
	}

//	private void getProductNum(ProductDO productDO) {
//		if (productDO.getProductNum() == null) {
//			productDO.setProductNum(sequenceGeneratorUtil.generateNextSeq("productNum"));
//		}
//	}


//	private void getClientID(List<ProductClientDO> productClientList) {
//		for (ProductClientDO productClientDO : productClientList) {
//			if (productClientDO.getClientID() == null) {
//				productClientDO.setClientID(sequenceGeneratorUtil.generateNextSeq("clientId"));
//			}
//		}
//
//	}

	@Override
	public ServiceDO getService() {
		ServiceDO serviceDO = new ServiceDO();
		serviceDO.setService("PRODUCT");
		serviceDO.setServiceType("Workflow");
		Date d= new Date(); 
		java.sql.Date dt = new java.sql.Date(d.getTime()); 
 		serviceDO.setServiceDt(dt);
//		serviceDO.setGuid(GuidGeneratorUtility.getInstance().generateGuid());
		ServiceTaskDO task = new ServiceTaskDO();
		task.setServiceTask("init");
		serviceDO.getServiceTaskList().add(task);
		
		return serviceDO;
	}

	}
