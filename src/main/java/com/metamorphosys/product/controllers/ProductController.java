package com.metamorphosys.product.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;
import com.metamorphosys.insureconnect.jpa.master.ProductRepository;
import com.metamorphosys.product.Executor.ProductExecutor;
import com.metamorphosys.product.interfaceobjects.CreateProductIO;
import com.metamorphosys.product.utility.SerializationUtility;

@RestController
@RequestMapping("/api/product")
final class ProductController {

	private static final Logger LOGGER =  LoggerFactory.getLogger(ProductController.class);
	
	private final ProductExecutor productExecutor;
	
	@Autowired
	ProductRepository productRepository;
	
	
	@Autowired
	ProductController (ProductExecutor productExecutor) {
        this.productExecutor = productExecutor;
    }
	
	@CrossOrigin
	@RequestMapping(value = "/init", method = RequestMethod.GET)
	ResponseEntity init(){
		//LOGGER.info("init ProductController");
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowMethods();
//		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		CreateProductIO io = new CreateProductIO();
		ProductDO productDO= new ProductDO();
		io.setProductDO(productDO);
		io=productExecutor.init(io);
		//LOGGER.info(SerializationUtility.getInstance().toJson(io));
		return new ResponseEntity(io, httpHeaders, httpStatus);		
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity save(@RequestBody @Valid CreateProductIO io){
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		//httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		//LOGGER.info("save ProductController");
		io=productExecutor.execute(io);
		
		return new ResponseEntity(io, httpHeaders, httpStatus);			
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	ResponseEntity fetch(@PathVariable("id") Long id) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		//LOGGER.info("fetch ProductController");
		
		ProductDO productDO = new ProductDO();
		productDO.setId(id);
		CreateProductIO io = new CreateProductIO();
		io.setProductDO(productDO);
		io=productExecutor.load(io);
		
		return new ResponseEntity(io, httpHeaders, httpStatus);	
		
	}
	
	@RequestMapping(value = "/fetchByPlantype/{plantype}", method = RequestMethod.GET)
	ResponseEntity fetchByPlantype(@PathVariable("plantype") String plantype) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
	//	LOGGER.info("fetch ProductController");
		List<ProductDO> productDOList= new ArrayList<ProductDO>();
				
		productDOList=productRepository.findByPlantype(plantype);
		
		return new ResponseEntity(productDOList, httpHeaders, httpStatus);	
		
	}
	
	@RequestMapping(value = "/searchwithcriteria/{procriteriaList}", method = RequestMethod.GET)
	ResponseEntity fetchByCriteria(@PathVariable("procriteriaList") ArrayList<String> procriteriaList) {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		//LOGGER.info("fetch ProductController");
		List<ProductDO> productDOList= new ArrayList<ProductDO>();
				
		productDOList=productRepository.findByProductNameOrProductFamilyCdOrPlantypeOrProductStatus(procriteriaList.get(0), procriteriaList.get(1), procriteriaList.get(2), procriteriaList.get(3));
//		productDOList=productRepository.findByProductNameAndProductFamilyCd(procriteriaList.get(0), procriteriaList.get(1));
//		productDOList=productRepository.findByProductNameOrProductFamilyCd(procriteriaList.get(0), procriteriaList.get(1));

		String productName=procriteriaList.get(0);
		String productFamilyCd=procriteriaList.get(1);
		String plantype=procriteriaList.get(2);
		String status=procriteriaList.get(3);
		
		
		return new ResponseEntity(productDOList, httpHeaders, httpStatus);	
		
	}
	
	
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	ResponseEntity fetchAllProducts() {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
	//	LOGGER.info("fetch ProductController");
		List<ProductDO> productDOList= new ArrayList<ProductDO>();
				
		productDOList=(List<ProductDO>) productRepository.findAll();
		
		return new ResponseEntity(productDOList, httpHeaders, httpStatus);	
		
	}
}

