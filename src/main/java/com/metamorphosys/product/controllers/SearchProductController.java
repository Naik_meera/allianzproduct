package com.metamorphosys.product.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;
import com.metamorphosys.insureconnect.jpa.master.ProductRepository;

//import com.metamorph.product.dataobjects.ProductDO;
//import com.metamorph.product.repository.ProductRepository;

@RestController
@RequestMapping("/api/searchProduct")
public class SearchProductController {

	private static final Logger LOGGER =  LoggerFactory.getLogger(SearchProductController.class);
	
	@Autowired
	ProductRepository productRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity fetch() {
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		LOGGER.info("Fetch SearchProductController");
		
		List<ProductDO> productList =(List<ProductDO>) productRepository.findAll();
		
		return new ResponseEntity(productList, httpHeaders, httpStatus);	
	}
	
}
