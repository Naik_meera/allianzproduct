package com.metamorphosys.product.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.internal.LinkedTreeMap;
import com.metamorphosys.insureconnect.common.interfaceobjects.RequestDataIO;
import com.metamorphosys.insureconnect.controllers.WebExecuteController;
import com.metamorphosys.insureconnect.dataobjects.master.ProductAttributeDO;
import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceTaskDO;
import com.metamorphosys.insureconnect.jpa.master.ProductRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.product.dataobjects.common.ExceptionDO;
import com.metamorphosys.product.dataobjects.common.ResultDO;
import com.metamorphosys.product.interfaceobjects.BaseIO;
import com.metamorphosys.product.interfaceobjects.ResponseIO;
import com.metamorphosys.product.utility.SerializationUtility;

@RestController
@RequestMapping("/api/validateProduct")
public class ValidateProduct {

	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	WebExecuteController webExecuteController;

	@RequestMapping(value = "/{productCd}" , method = RequestMethod.POST)
	public String validateComputeProduct(@RequestBody @Valid  String baseIOJson, @PathVariable String productCd)
			throws ClassNotFoundException {
		RequestDataIO baseIO=(RequestDataIO) SerializationUtility.getInstance().fromJson(baseIOJson,RequestDataIO.class);
		ProductDO product = productRepository.findByProductId(productCd);
		ResponseIO responseIO = new ResponseIO();
		responseIO =  riderAssociation(product, baseIO,responseIO);
		responseIO =  validateProduct(product, baseIO,responseIO);
		if(responseIO.getResultDO()==null || (responseIO.getResultDO()!=null && responseIO.getResultDO().getExceptionList().size()==0)){
			responseIO = computeProduct(product, baseIO,responseIO);
		}
		responseIO = getUIProperty(baseIO,responseIO);
		String json = SerializationUtility.getInstance().toJson(responseIO);
		return json;
	}

	private ResponseIO riderAssociation(ProductDO product, RequestDataIO baseIO, ResponseIO responseIO) {
		for (ProductAttributeDO productAttributeDO : product.getAttributeList()) {
			if ("RiderAssociation".equals(productAttributeDO.getAttributeTypeCd())) {
				String json = covertTojson(baseIO, createService(productAttributeDO, product.getProductId()));
				responseIO =callRuleEngine(json, baseIO.getObjectName(), responseIO);
				baseIO.setObject(responseIO.getBaseDO());
			}
		}
		return responseIO;
	}

	private ResponseIO getUIProperty(RequestDataIO baseIO, ResponseIO io) {
		String json = covertTojson(baseIO, createUIService(baseIO));
		io =callRuleEngine(json, baseIO.getObjectName(), io);
		baseIO.setObject(io.getBaseDO());
		return io;
	}

	private ServiceDO createUIService(RequestDataIO baseIO) {
		ServiceDO serviceDO = new ServiceDO();
		serviceDO.setService("Product");
		serviceDO.setServiceType("UI");
		ServiceTaskDO serviceTaskDO = new ServiceTaskDO();
		serviceTaskDO.setServiceTask(baseIO.getObjectName()+"UI");
		serviceDO.addServiceTask(serviceTaskDO);
		return serviceDO;
	}

	private ResponseIO computeProduct(ProductDO product, RequestDataIO baseIO, ResponseIO io) {
		for (ProductAttributeDO productAttributeDO : product.getAttributeList()) {
			if (InsureConnectConstants.AttributeType.PRODUCTCALCULATION.equals(productAttributeDO.getAttributeTypeCd())) {
				String json = covertTojson(baseIO, createService(productAttributeDO, product.getProductId()));
				io =callRuleEngine(json, baseIO.getObjectName(), io);
				baseIO.setObject(io.getBaseDO());
			}
		}
		return io;
	}

	private ResponseIO validateProduct(ProductDO product, RequestDataIO baseIO, ResponseIO io) {
		for (ProductAttributeDO productAttributeDO : product.getAttributeList()) {
			if (InsureConnectConstants.AttributeType.PRODUCTVALIDATION.equals(productAttributeDO.getAttributeTypeCd())) {
				String json = covertTojson(baseIO, createService(productAttributeDO, product.getProductId()));
				io =callRuleEngine(json, baseIO.getObjectName(),io);
				baseIO.setObject(io.getBaseDO());
			}
		}
		return io;
	}

	private ResponseIO callRuleEngine(String json, String objectName, ResponseIO responseIO) {
		/*RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(json.toString(), httpHeaders);
		ResponseEntity<String> jsonObject = restTemplate.exchange("http://localhost:8081/InsureConnect/execute",
				HttpMethod.POST, entity, String.class);
		responseIO = convertjsonToObject(jsonObject.getBody(), responseIO, objectName);
*/
		ResponseEntity<String> jsonObject=webExecuteController.executeJson(json);
		responseIO = convertjsonToObject(jsonObject.getBody(), responseIO, objectName);
		
		return responseIO;
	}

	private ResponseIO convertjsonToObject(String body, ResponseIO io, String objectName) {
		String data = null;
		LinkedTreeMap map = SerializationUtility.getInstance().fromJson(body);

		if (null != map && map.containsKey("data")) {
			data = map.get("data").toString();
		}

		ArrayList<LinkedTreeMap> responseDataList = (ArrayList<LinkedTreeMap>) SerializationUtility.getInstance().fromJson(data, ArrayList.class);
		if (null != responseDataList) {
			for (LinkedTreeMap responseDataIO : responseDataList) {
				if (InsureConnectConstants.DataObjects.SERVICEDO.equals(responseDataIO.get("objectName").toString())) {
					ServiceDO serviceDO = (ServiceDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ServiceDO.class);

					io.setServiceDO(serviceDO);
				} else if (InsureConnectConstants.DataObjects.RESULTDO.equals(responseDataIO.get("objectName").toString())) {
					ResultDO resultDO = (ResultDO) SerializationUtility.getInstance()
							.fromJson(responseDataIO.get("object").toString(), ResultDO.class);
					if(io.getResultDO()==null){
						io.setResultDO(resultDO);
					}else{
						if(io.getResultDO().getExceptionList()==null && io.resultDO.getExceptionList()!=null){
							io.getResultDO().setExceptionList(resultDO.getExceptionList());
						}else if(io.getResultDO().getExceptionList()!=null && io.resultDO.getExceptionList()!=null){
							List<ExceptionDO> exceptionDOs = new ArrayList<>();
							exceptionDOs.addAll(resultDO.getExceptionList());
							exceptionDOs.addAll(io.getResultDO().getExceptionList());
							io.getResultDO().setExceptionList(exceptionDOs);
							
						}
						if(io.getResultDO().getResultMap()==null && resultDO.getResultMap()!=null){
							io.getResultDO().setResultMap(resultDO.getResultMap());
						}else if(io.getResultDO().getResultMap()!=null && resultDO.getResultMap()!=null){
							io.getResultDO().getResultMap().putAll(resultDO.getResultMap());
						}
					}
				} else if (objectName != null && objectName.equals(responseDataIO.get("objectName").toString())) {
					io.setBaseDO(responseDataIO.get("object").toString());
				}
			}
		}
		return io;

	}


	private ServiceDO createService(ProductAttributeDO productAttributeDO, String productId) {
		ServiceDO serviceDO = new ServiceDO();
		serviceDO.setService("Product");
		Date d= new Date(); 
		java.sql.Date dt = new java.sql.Date(d.getTime()); 
 		serviceDO.setServiceDt(dt);
		serviceDO.setServiceType(productAttributeDO.getAttributeTypeCd());
		ServiceTaskDO serviceTask = new ServiceTaskDO();
		serviceTask.setServiceTask(productId + productAttributeDO.getAttributeId());
		serviceDO.addServiceTask(serviceTask);

		return serviceDO;
	}

	public String covertTojson(RequestDataIO baseIO, ServiceDO serviceDO) {
		List<RequestDataIO> baseIOs = new ArrayList<RequestDataIO>();

		if (baseIO != null)
			baseIOs.add(baseIO);

		if (serviceDO != null) {
			RequestDataIO baseIO1 = new RequestDataIO();
			baseIO1.setObjectName(serviceDO.getObjectName());
			baseIO1.setObject(SerializationUtility.getInstance().toJson(serviceDO));
			baseIOs.add(baseIO1);
		}

		String serialize = SerializationUtility.getInstance().toJson(baseIOs);
		HashMap<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("data", serialize);
		String finalString = SerializationUtility.getInstance().toJson(dataMap);

		return finalString;

	}

}
