package com.metamorphosys.product.interfaceobjects;

import com.metamorphosys.insureconnect.dataobjects.master.ProductDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.product.dataobjects.common.ResultDO;



public class CreateProductIO {

	public ProductDO productDO;
	
//	public List<ProductDO> productDOList;
	
	
	
	public ServiceDO  serviceDO;
	
	public ResultDO resultDO;	

	public ProductDO getProductDO() {
		return productDO;
	}

	public void setProductDO(ProductDO productDO) {
		this.productDO = productDO;
	}

//	public List<ProductDO> getProductDOList() {
//		return productDOList;
//	}
//
//	public void setProductDOList(List<ProductDO> productDOList) {
//		this.productDOList = productDOList;
//	}

	public ServiceDO getServiceDO() {
		return serviceDO;
	}

	public void setServiceDO(ServiceDO serviceDO) {
		this.serviceDO = serviceDO;
	}

	public ResultDO getResultDO() {
		return resultDO;
	}

	public void setResultDO(ResultDO resultDO) {
		this.resultDO = resultDO;
	}

		
}
