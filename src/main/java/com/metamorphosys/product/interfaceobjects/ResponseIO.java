package com.metamorphosys.product.interfaceobjects;

import com.metamorphosys.insureconnect.dataobjects.transaction.ServiceDO;
import com.metamorphosys.product.dataobjects.common.ResultDO;

public class ResponseIO {
	
	public String baseDO;

	public ServiceDO serviceDO;
	
	public ResultDO resultDO;

	public String getBaseDO() {
		return baseDO;
	}

	public void setBaseDO(String baseDO) {
		this.baseDO = baseDO;
	}

	public ServiceDO getServiceDO() {
		return serviceDO;
	}

	public void setServiceDO(ServiceDO serviceDO) {
		this.serviceDO = serviceDO;
	}

	public ResultDO getResultDO() {
		return resultDO;
	}

	public void setResultDO(ResultDO resultDO) {
		this.resultDO = resultDO;
	}
	
	
}
